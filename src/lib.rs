use chrono::Duration;
use figlet_rs::FIGfont;
use nvim_oxi::api::{self, types::*, Window};
use nvim_oxi::{self as oxi, lua, Dictionary, Function, Object};
use oxi::api::Buffer;
use oxi::conversion::{self, FromObject, ToObject};
use oxi::serde::{Deserializer, Serializer};
use serde::{Deserialize, Serialize};
use std::borrow::{Borrow, BorrowMut};
use std::cell::RefCell;
use std::error::Error;
use std::ops::Deref;
use std::rc::Rc;
use std::thread::sleep;

#[derive(Serialize, Deserialize, Clone)]
struct Reminder {
    message: String,
    work_duration: i64,
    rest_duration: i64,
    enabled: bool,
    initial_state: String,
}

// Boilerplate code for Lua to Rust data type conversion

impl FromObject for Reminder {
    fn from_object(obj: Object) -> Result<Self, conversion::Error> {
        Self::deserialize(Deserializer::new(obj)).map_err(Into::into)
    }
}

impl ToObject for Reminder {
    fn to_object(self) -> Result<oxi::Object, conversion::Error> {
        self.serialize(Serializer::new()).map_err(Into::into)
    }
}

impl lua::Pushable for Reminder {
    unsafe fn push(self, lstate: *mut lua::ffi::lua_State) -> Result<std::ffi::c_int, lua::Error> {
        self.to_object()
            .map_err(lua::Error::push_error_from_err::<Self, _>)?
            .push(lstate)
    }
}

impl lua::Poppable for Reminder {
    unsafe fn pop(lstate: *mut lua::ffi::lua_State) -> Result<Self, lua::Error> {
        let obj = Object::pop(lstate)?;
        Self::from_object(obj).map_err(lua::Error::pop_error_from_err::<Self, _>)
    }
}

fn start_session(reminder: Reminder) -> oxi::Result<Reminder> {
    let mut cloned_reminder = reminder.clone();
    let reminder_message = &reminder.message.as_str();
    let banner = generate_ascii_banner(reminder_message);
    let window = create_window().unwrap();
    let buffer = create_buffer(&banner);

    match cloned_reminder.initial_state.as_str() {
        "resting" => {
            let duration = Duration::minutes(reminder.work_duration);
            sleep(duration.to_std().unwrap());
            cloned_reminder.initial_state = "working".to_string();
            let _ = open_window(window.clone(), buffer);
            let _ = start_session(cloned_reminder.clone()); // recursively calls itself for sessions to continue to alternate
        }
        "working" => {
            let duration = Duration::minutes(reminder.rest_duration);
            sleep(duration.to_std().unwrap());
            cloned_reminder.initial_state = "resting".to_string();
            let _ = close_window(window.clone());
            let _ = start_session(cloned_reminder.clone()); // recursively calls itself for sessions to continue to alternate
        }
        _ => {
            panic!("Initial state not provided")
        }
    }

    Ok(cloned_reminder)
}

fn create_window() -> Result<Rc<RefCell<Option<Window>>>, Box<dyn Error>> {
    let win: Rc<RefCell<Option<Window>>> = Rc::default();

    let w = Rc::clone(&win);

    Ok(w)
}

fn create_buffer(content: &str) -> Buffer {
    let mut buf = api::create_buf(false, true).unwrap();
    let _ = buf.set_lines(0..6, false, content.to_owned().chars());
    buf
}

fn open_window(w: Rc<RefCell<Option<Window>>>, buf: Buffer) -> Result<(), Box<dyn Error>> {
    let win_ref = Rc::clone(&w);
    if win_ref.deref().borrow().is_some() {
        api::err_writeln("Window is already open");
        return Ok(());
    }

    let config = WindowConfig::builder()
        .relative(WindowRelativeTo::Cursor)
        .height(10)
        .width(20)
        .row(20)
        .col(40)
        .build();

    // let mut win = Rc::clone(&w);
    // let win_mut = win.borrow_mut();
    let win = Some(api::open_win(&buf, false, &config)?);
    Ok(())
}

fn close_window(window: Rc<RefCell<Option<Window>>>) -> Result<(), Box<dyn Error>> {
    let mut win_ref = Rc::clone(&window);
    if win_ref.deref().borrow().is_none() {
        api::err_writeln("Window is already closed");
        return Ok(());
    }

    let win = win_ref.borrow_mut().take().unwrap();
    let _ = win.close(true);
    Ok(())
}

fn generate_ascii_banner(content: &str) -> String {
    let standard_font = FIGfont::standard().unwrap();
    let figure = standard_font.convert(content);
    match figure {
        Some(figure) => figure.to_string(),
        None => {
            println!("Failed to convert {}", content);
            standard_font.convert("Take a break!").unwrap().to_string()
        }
    }
}

#[nvim_oxi::module]
fn nvim_restremind() -> oxi::Result<Dictionary> {
    Ok(Dictionary::from_iter([(
        "start_session",
        Function::from_fn(start_session),
    )]))
}
